package com.rhkiswani.dawacom.rest;

import com.rhkiswani.api.consumer.rest.AbstractRestClient;
import com.rhkiswani.api.consumer.rest.LightAdminAbstractRestClient;
import com.rhkiswani.api.consumer.rest.StrongLoopAbstractRestClient;
import com.rhkiswani.api.consumer.rest.exceptions.RestException;
import com.rhkiswani.dawacom.models.InquiriesCategories;

public class InquiriesCategoriesRestClient extends StrongLoopAbstractRestClient<InquiriesCategories> {
	
	public InquiriesCategoriesRestClient() {
		super("InquiriesCategories");
	}

	public static void main(String[] args) {
		System.setProperty(AbstractRestClient.API_BASE_URL_PROP, "http://52.24.207.107:3000/");
		InquiriesCategoriesRestClient inquiriesCategoriesRestClient = new InquiriesCategoriesRestClient();
		try {
			System.out.println(inquiriesCategoriesRestClient.list().size());
		} catch (RestException e) {
			e.printStackTrace();
		}
	}

}
