package com.rhkiswani.dawacom.rest;

import java.util.List;
import java.util.Map;

import com.rhkiswani.api.consumer.rest.AbstractRestClient;
import com.rhkiswani.api.consumer.rest.LightAdminAbstractRestClient;
import com.rhkiswani.api.consumer.rest.StrongLoopAbstractRestClient;
import com.rhkiswani.api.consumer.rest.exceptions.RestException;
import com.rhkiswani.dawacom.models.Inquiries;

public class InquiriesRestClient extends StrongLoopAbstractRestClient<Inquiries> {
	
	public InquiriesRestClient() {
		super("Inquiries");
	}


	public static void main(String[] args) {
		System.setProperty(AbstractRestClient.API_BASE_URL_PROP, "http://52.24.207.107:3000/");
		InquiriesRestClient inquiriesRestClient = new InquiriesRestClient();
		try {
			List<Inquiries> list = inquiriesRestClient.list();
			System.out.println(list.get(0).getInquiriesCategoryId());
		} catch (RestException e) {
			e.printStackTrace();
		}
	}
}
