package com.rhkiswani.dawacom.rest;

import java.util.List;

import com.rhkiswani.api.consumer.rest.AbstractRestClient;
import com.rhkiswani.api.consumer.rest.LightAdminAbstractRestClient;
import com.rhkiswani.api.consumer.rest.StrongLoopAbstractRestClient;
import com.rhkiswani.api.consumer.rest.exceptions.RestException;
import com.rhkiswani.dawacom.models.ClientQuestionsAnswers;

public class ClientQuestionsAnswerRestClient extends StrongLoopAbstractRestClient<ClientQuestionsAnswers> {

	public ClientQuestionsAnswerRestClient() {
		super("ClientQuestions");
	}

	public static void main(String[] args) throws RestException {
		System.setProperty(AbstractRestClient.API_BASE_URL_PROP, "http://52.24.207.107:3000/");
		ClientQuestionsAnswerRestClient client = new ClientQuestionsAnswerRestClient();
		List<ClientQuestionsAnswers> list = client.list();
		for (ClientQuestionsAnswers obj : list) {
			System.out.println(obj);
		}
	}

}
