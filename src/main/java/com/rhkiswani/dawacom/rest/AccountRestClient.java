package com.rhkiswani.dawacom.rest;

import java.util.Date;
import java.util.List;

import com.rhkiswani.api.consumer.rest.AbstractRestClient;
import com.rhkiswani.api.consumer.rest.StrongLoopAbstractRestClient;
import com.rhkiswani.api.consumer.rest.exceptions.RestException;
import com.rhkiswani.api.consumer.util.Filters;
import com.rhkiswani.dawacom.models.Accounts;

public class AccountRestClient extends StrongLoopAbstractRestClient<Accounts> {

	public AccountRestClient() {
		super("Accounts");
	}

	public static void main(String[] args) throws RestException {
		System.setProperty(AbstractRestClient.API_BASE_URL_PROP,"http://52.24.207.107:3000/");
		AccountRestClient client = new AccountRestClient();
//		List<Accounts> list = client.list();
//		for (Accounts obj : list) {
//			System.out.println(obj);
//		}
//		Accounts accounts = new Accounts();
//		accounts.setCreatedDate(new Date());
//		String deviceId = "123";
//		accounts.setEmail(deviceId+"@dawacom.com");
//		accounts.setPassword(deviceId);
//		accounts.setDeviceId(deviceId);
//		accounts.setGcmId("asdf");
//		client.create(accounts);
		Accounts find = client.findSingle(Filters.newInstance().add("deviceId","kiswani"));
		
	}

}
