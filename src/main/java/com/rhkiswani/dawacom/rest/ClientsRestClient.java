package com.rhkiswani.dawacom.rest;

import java.util.List;

import com.rhkiswani.api.consumer.rest.AbstractRestClient;
import com.rhkiswani.api.consumer.rest.LightAdminAbstractRestClient;
import com.rhkiswani.api.consumer.rest.StrongLoopAbstractRestClient;
import com.rhkiswani.api.consumer.rest.exceptions.RestException;
import com.rhkiswani.dawacom.models.Clients;

public class ClientsRestClient extends StrongLoopAbstractRestClient<Clients> {

	public ClientsRestClient() {
		super("Clients");
	}

	public static void main(String[] args) throws RestException {
		System.setProperty(AbstractRestClient.API_BASE_URL_PROP, "http://52.24.207.107:3000/");
		ClientsRestClient client = new ClientsRestClient();
		List<Clients> list = client.list();
		for (Clients obj : list) {
			System.out.println(obj);
		}
	}

}
