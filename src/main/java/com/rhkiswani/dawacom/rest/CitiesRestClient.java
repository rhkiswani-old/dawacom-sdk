package com.rhkiswani.dawacom.rest;

import java.util.List;

import com.rhkiswani.api.consumer.rest.AbstractRestClient;
import com.rhkiswani.api.consumer.rest.LightAdminAbstractRestClient;
import com.rhkiswani.api.consumer.rest.StrongLoopAbstractRestClient;
import com.rhkiswani.api.consumer.rest.exceptions.RestException;
import com.rhkiswani.dawacom.models.Adviaces;
import com.rhkiswani.dawacom.models.Cities;
public class CitiesRestClient extends StrongLoopAbstractRestClient<Cities> {

	
	public CitiesRestClient() {
		super("Cities");
	}

	public static void main(String[] args) throws RestException {
		System.setProperty(AbstractRestClient.API_BASE_URL_PROP, "http://52.24.207.107:3000");
		CitiesRestClient client = new CitiesRestClient();
		List<Cities> list = client.list();
		for (Cities obj : list) {
			System.out.println(obj.getCityName());
		}
	}

}
