package com.rhkiswani.dawacom.rest;

import java.util.List;

import com.rhkiswani.api.consumer.rest.AbstractRestClient;
import com.rhkiswani.api.consumer.rest.StrongLoopAbstractRestClient;
import com.rhkiswani.api.consumer.rest.exceptions.RestException;
import com.rhkiswani.api.consumer.util.Filters;
import com.rhkiswani.dawacom.models.Cities;
import com.rhkiswani.dawacom.models.Pharmacies;

public class PharmaciesRestClient extends StrongLoopAbstractRestClient<Pharmacies> {


	public PharmaciesRestClient() {
		super("Pharmacies");
	}

	public static void main(String[] args) throws RestException {
		System.setProperty(AbstractRestClient.API_BASE_URL_PROP, "http://52.24.207.107:3000");
		CitiesRestClient client = new CitiesRestClient();
		List<Cities> cities = client.list();
		PharmaciesRestClient pclient = new PharmaciesRestClient();
		for (Cities cities2 : cities) {
			List<Pharmacies> listOfPhar = pclient.find(Filters.newInstance().add("cityId", cities2.getCityId() + ""));
			if (listOfPhar != null) {
				for (Pharmacies pharmacies : listOfPhar) {
					System.out.println(pharmacies.getPharmcyName());
				}
			}
		}
	}
}
