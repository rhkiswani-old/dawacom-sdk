package com.rhkiswani.dawacom.rest;

import java.util.List;

import com.rhkiswani.api.consumer.rest.AbstractRestClient;
import com.rhkiswani.api.consumer.rest.LightAdminAbstractRestClient;
import com.rhkiswani.api.consumer.rest.StrongLoopAbstractRestClient;
import com.rhkiswani.api.consumer.rest.exceptions.RestException;
import com.rhkiswani.dawacom.models.ClientQuestions;
import com.rhkiswani.dawacom.models.PharmcyMembers;

public class ClientQuestionsRestClient extends StrongLoopAbstractRestClient<ClientQuestions> {

	public ClientQuestionsRestClient() {
		super("ClientQuestions");
	}

	public static void main(String[] args) throws RestException {
		System.setProperty(AbstractRestClient.API_BASE_URL_PROP, "http://52.24.207.107:3000/");
		ClientQuestionsRestClient client = new ClientQuestionsRestClient();
		List<ClientQuestions> list = client.list();
		for (ClientQuestions obj : list) {
			System.out.println(obj);
		}
	}

}
