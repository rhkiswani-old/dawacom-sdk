package com.rhkiswani.dawacom.rest;

import java.util.List;

import com.rhkiswani.api.consumer.rest.AbstractRestClient;
import com.rhkiswani.api.consumer.rest.StrongLoopAbstractRestClient;
import com.rhkiswani.api.consumer.rest.exceptions.RestException;
import com.rhkiswani.dawacom.models.Offer;

public class OffersRestClient extends StrongLoopAbstractRestClient<Offer> {

	public OffersRestClient() {
		super("Offers");
	}

//	@Override
//	public List<Offer> list() throws RestException {
//		String result = RestHelper.get(getApiURL() + "/getOffers");
//		GsonBuilder builder = new GsonBuilder();
//		builder.registerTypeAdapter(OfferType.class, new JsonDeserializer<OfferType>() {
//			public OfferType deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
//				OfferType type = new Gson().fromJson(json, OfferType.class);
//				type.setOfferTypeImage(getApiURL()+"/offerTypes/"+type.getOfferTypeId()+"/offerTypeImage/file");
//				return type;
//			}
//		});
//		builder.registerTypeAdapter(Date.class, new JsonDeserializer<Date>() {
//			public Date deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
//				return new Date(json.getAsJsonPrimitive().getAsLong());
//			}
//		});
//		Gson gson = builder.create();
//		Offer[] offers = gson.fromJson(result, Offer[].class);
//		for (Offer offer : offers) {
//			offer.setDefaultImage(getApiURL()+"/offers/"+offer.getOfferId()+"/defaultImage");
//			System.out.println(getApiURL()+"/offers/"+offer.getOfferId()+"/defaultImage");
//		}
//		return Arrays.asList(offers);
//	}

	public static void main(String[] args) throws RestException {
		System.setProperty(AbstractRestClient.API_BASE_URL_PROP, "http://52.24.207.107:3000/");
		OffersRestClient client = new OffersRestClient();
		List<Offer> list = client.list();
		for (Offer obj : list) {
			System.out.println(obj.getOfferTypeId());
		}
	}

}
