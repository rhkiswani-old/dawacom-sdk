package com.rhkiswani.dawacom.rest;

import java.util.List;

import com.rhkiswani.api.consumer.rest.AbstractRestClient;
import com.rhkiswani.api.consumer.rest.LightAdminAbstractRestClient;
import com.rhkiswani.api.consumer.rest.StrongLoopAbstractRestClient;
import com.rhkiswani.api.consumer.rest.exceptions.RestException;
import com.rhkiswani.dawacom.models.PharmcyMembers;

public class PharmcyMemberRestClient extends StrongLoopAbstractRestClient<PharmcyMembers> {

	public PharmcyMemberRestClient() {
		super("PharmcyMembers");
	}

	public static void main(String[] args) throws RestException {
		System.setProperty(AbstractRestClient.API_BASE_URL_PROP, "http://52.24.207.107:3000/");
		PharmcyMemberRestClient client = new PharmcyMemberRestClient();
		List<PharmcyMembers> list = client.list();
		for (PharmcyMembers weekDays : list) {
			System.out.println(weekDays);
		}
	}
}
