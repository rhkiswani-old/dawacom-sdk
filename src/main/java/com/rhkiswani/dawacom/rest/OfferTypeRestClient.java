package com.rhkiswani.dawacom.rest;

import java.util.List;

import com.rhkiswani.api.consumer.rest.AbstractRestClient;
import com.rhkiswani.api.consumer.rest.StrongLoopAbstractRestClient;
import com.rhkiswani.api.consumer.rest.exceptions.RestException;
import com.rhkiswani.dawacom.models.OfferType;

public class OfferTypeRestClient extends StrongLoopAbstractRestClient<OfferType> {

	public OfferTypeRestClient() {
		super("OfferTypes");
	}

	public static void main(String[] args) throws RestException {
		System.setProperty(AbstractRestClient.API_BASE_URL_PROP, "http://52.24.207.107:3000/");
		OfferTypeRestClient client = new OfferTypeRestClient();
		List<OfferType> list = client.list();
		for (OfferType obj : list) {
			System.out.println(obj.getOfferTypeId());
		}
	}
}
