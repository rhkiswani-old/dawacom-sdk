package com.rhkiswani.dawacom.rest;

import java.util.List;

import com.rhkiswani.api.consumer.rest.AbstractRestClient;
import com.rhkiswani.api.consumer.rest.LightAdminAbstractRestClient;
import com.rhkiswani.api.consumer.rest.StrongLoopAbstractRestClient;
import com.rhkiswani.api.consumer.rest.exceptions.RestException;
import com.rhkiswani.dawacom.models.ScientificDegrees;

public class ScientificDegreesRestClient extends StrongLoopAbstractRestClient<ScientificDegrees> {

	public ScientificDegreesRestClient() {
		super("ScientificDegrees");
	}

	public static void main(String[] args) throws RestException {
		System.setProperty(AbstractRestClient.API_BASE_URL_PROP, "http://52.24.207.107:3000/");
		ScientificDegreesRestClient client = new ScientificDegreesRestClient();
		List<ScientificDegrees> list = client.list();
		for (ScientificDegrees weekDays : list) {
			System.out.println(weekDays);
		}
	}
}
