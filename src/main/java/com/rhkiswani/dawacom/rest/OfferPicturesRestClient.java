package com.rhkiswani.dawacom.rest;

import java.util.List;

import com.rhkiswani.api.consumer.rest.AbstractRestClient;
import com.rhkiswani.api.consumer.rest.StrongLoopAbstractRestClient;
import com.rhkiswani.api.consumer.rest.exceptions.RestException;
import com.rhkiswani.api.consumer.util.Filters;
import com.rhkiswani.dawacom.models.OfferPictures;

public class OfferPicturesRestClient extends StrongLoopAbstractRestClient<OfferPictures> {
	
	public OfferPicturesRestClient() {
		super("OffcerPictures");
	}

	
	public List<OfferPictures> getOfferPictures(int offerId) throws RestException{
		return find(Filters.newInstance().add("offerId", offerId+""));
	}
	
	public static void main(String[] args) throws RestException {
		System.setProperty(AbstractRestClient.API_BASE_URL_PROP, "http://52.24.207.107:3000");
		OfferPicturesRestClient client = new OfferPicturesRestClient();
//		List<OfferPictures> list = client.list();
//		for (OfferPictures obj : list) {
//			System.out.println(obj.getOfferPicture());
//		}
		List<OfferPictures>	 list = client.list();
		for (OfferPictures obj : list) {
			byte[] offerPicture = obj.getOfferPicture();
			//System.out.println(obj.get);
		}
	}

}
