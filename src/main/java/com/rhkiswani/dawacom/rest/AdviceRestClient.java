package com.rhkiswani.dawacom.rest;

import java.util.List;

import com.rhkiswani.api.consumer.rest.AbstractRestClient;
import com.rhkiswani.api.consumer.rest.LightAdminAbstractRestClient;
import com.rhkiswani.api.consumer.rest.StrongLoopAbstractRestClient;
import com.rhkiswani.api.consumer.rest.exceptions.RestException;
import com.rhkiswani.dawacom.models.Accounts;
import com.rhkiswani.dawacom.models.Adviaces;
import com.rhkiswani.dawacom.models.PharmcyMembers;

public class AdviceRestClient extends StrongLoopAbstractRestClient<Adviaces> {

	public AdviceRestClient() {
		super("Adviaces");
	}

	public static void main(String[] args) throws RestException {
		System.setProperty(AbstractRestClient.API_BASE_URL_PROP, "http://52.24.207.107:3000/");
		AdviceRestClient client = new AdviceRestClient();
		List<Adviaces> list = client.list();
		for (Adviaces obj : list) {
			System.out.println(obj.getAdviceImg());
		}
	}

}
