package com.rhkiswani.dawacom.rest;

import java.util.List;

import com.rhkiswani.api.consumer.rest.AbstractRestClient;
import com.rhkiswani.api.consumer.rest.LightAdminAbstractRestClient;
import com.rhkiswani.api.consumer.rest.StrongLoopAbstractRestClient;
import com.rhkiswani.api.consumer.rest.exceptions.RestException;
import com.rhkiswani.api.consumer.rest.triggers.CrudTrigger;
import com.rhkiswani.api.consumer.rest.triggers.CrudTriggerAdapter;
import com.rhkiswani.api.consumer.rest.triggers.TriggersManager;
import com.rhkiswani.dawacom.models.WeekDays;

public class WeekDaysRestClient extends StrongLoopAbstractRestClient<WeekDays> {

	public WeekDaysRestClient() {
		super("WeekDays");
	}

	public static void main(String[] args) throws RestException {
		System.setProperty(AbstractRestClient.API_BASE_URL_PROP,"http://52.24.207.107:3000/");
		WeekDaysRestClient client = new WeekDaysRestClient();
		List<WeekDays> list = client.list();
		for (WeekDays weekDays : list) {
			System.out.println(weekDays);
		}
	}
}
