package com.rhkiswani.dawacom.rest;

import java.util.List;

import com.rhkiswani.api.consumer.rest.AbstractRestClient;
import com.rhkiswani.api.consumer.rest.LightAdminAbstractRestClient;
import com.rhkiswani.api.consumer.rest.StrongLoopAbstractRestClient;
import com.rhkiswani.api.consumer.rest.exceptions.RestException;
import com.rhkiswani.dawacom.models.PharmcyWorkingDays;

public class PharmcyWorkingDaysClient extends StrongLoopAbstractRestClient<PharmcyWorkingDays> {

	public PharmcyWorkingDaysClient() {
		super("PharmcyWorkingDays");
	}

	public static void main(String[] args) throws RestException {
		System.setProperty(AbstractRestClient.API_BASE_URL_PROP, "http://52.24.207.107:3000/");
		PharmcyWorkingDaysClient client = new PharmcyWorkingDaysClient();
		List<PharmcyWorkingDays> list = client.list();
		for (PharmcyWorkingDays weekDays : list) {
			System.out.println(weekDays);
		}
	}
}
