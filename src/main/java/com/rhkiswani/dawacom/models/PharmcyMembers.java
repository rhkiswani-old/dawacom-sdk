/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rhkiswani.dawacom.models;

import java.io.Serializable;
import java.util.Date;

import com.rhkiswani.api.consumer.rest.AbstractEntity;
import com.rhkiswani.api.consumer.rest.AbstractRestClient;
import com.rhkiswani.api.consumer.rest.annotations.IdField;
import com.rhkiswani.api.consumer.rest.exceptions.RestException;
import com.rhkiswani.dawacom.rest.PharmcyMemberRestClient;

/**
 *
 * @author mohamed
 */
public class PharmcyMembers extends AbstractEntity<PharmcyMembers> implements Serializable {
    private static final long serialVersionUID = 1L;
    @IdField
    private Integer memberId;
    private Integer pharmcyId;
    private String memberFirstName;
    private String memberLastName;
    private int isContactPerson;
    private Integer age;
    private String personalTel;
    private String personalAdress;
    private Date joinDate;
    private String personalEmail;
    private Integer degreeId;

    public PharmcyMembers() {
    }

    public PharmcyMembers(Integer memberId) {
        this.memberId = memberId;
    }

    public PharmcyMembers(Integer memberId, int pharmcyId, String memberFirstName, String memberLastName, Date joinDate) {
        this.memberId = memberId;
        this.pharmcyId = pharmcyId;
        this.memberFirstName = memberFirstName;
        this.memberLastName = memberLastName;
        this.joinDate = joinDate;
    }

    public Integer getMemberId() {
        return memberId;
    }

    public void setMemberId(Integer memberId) {
        this.memberId = memberId;
    }

    public Integer getPharmcyId() {
        return pharmcyId;
    }

    public void setPharmcyId(Integer pharmcyId) {
        this.pharmcyId = pharmcyId;
    }

    public String getMemberFirstName() {
        return memberFirstName;
    }

    public void setMemberFirstName(String memberFirstName) {
        this.memberFirstName = memberFirstName;
    }

    public String getMemberLastName() {
        return memberLastName;
    }

    public void setMemberLastName(String memberLastName) {
        this.memberLastName = memberLastName;
    }

    public int getIsContactPerson() {
        return isContactPerson;
    }

    public void setIsContactPerson(int isContactPerson) {
        this.isContactPerson = isContactPerson;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getPersonalTel() {
        return personalTel;
    }

    public void setPersonalTel(String personalTel) {
        this.personalTel = personalTel;
    }

    public String getPersonalAdress() {
        return personalAdress;
    }

    public void setPersonalAdress(String personalAdress) {
        this.personalAdress = personalAdress;
    }

    public Date getJoinDate() {
        return joinDate;
    }

    public void setJoinDate(Date joinDate) {
        this.joinDate = joinDate;
    }

    public String getPersonalEmail() {
        return personalEmail;
    }

    public void setPersonalEmail(String personalEmail) {
        this.personalEmail = personalEmail;
    }

    public Integer getDegreeId() {
        return degreeId;
    }

    public void setDegreeId(Integer degreeId) {
        this.degreeId = degreeId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (memberId != null ? memberId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PharmcyMembers)) {
            return false;
        }
        PharmcyMembers other = (PharmcyMembers) object;
        if ((this.memberId == null && other.memberId != null) || (this.memberId != null && !this.memberId.equals(other.memberId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.rhkiswani.dawacom.models.PharmcyMembers[ memberId=" + memberId + " ]";
    }

	@Override
	public AbstractRestClient<PharmcyMembers> getRestClient() throws RestException {
		return new PharmcyMemberRestClient();
	}

	@Override
	public String getURLpath() {
		return "/pharmcyMembers";
	}
    
}
