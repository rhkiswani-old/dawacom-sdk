/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rhkiswani.dawacom.models;

import java.io.Serializable;

import com.rhkiswani.api.consumer.rest.AbstractEntity;
import com.rhkiswani.api.consumer.rest.AbstractRestClient;
import com.rhkiswani.api.consumer.rest.annotations.IdField;
import com.rhkiswani.api.consumer.rest.exceptions.RestException;
import com.rhkiswani.dawacom.rest.OfferPicturesRestClient;

/**
 *
 * @author mohamed
 */

public class OfferPictures extends AbstractEntity<OfferPictures> implements Serializable {
    private static final long serialVersionUID = 1L;
    
   @IdField
    private Integer offerPicId;
   
    private Integer offerId;
  
    private byte[] offerPicture;
    
	public Integer getOfferPicId() {
		return offerPicId;
	}
	public void setOfferPicId(Integer offerPicId) {
		this.offerPicId = offerPicId;
	}
	public Integer getOfferId() {
		return offerId;
	}
	public void setOfferId(Integer offerId) {
		this.offerId = offerId;
	}
	public byte[] getOfferPicture() {
		return offerPicture;
	}
	public void setOfferPicture(byte[] offerPicture) {
		this.offerPicture = offerPicture;
	}
	@Override
	public String toString() {
		return "Offer Pictures#"+offerPicId;
	}
	
	
	@Override
	public AbstractRestClient<OfferPictures> getRestClient() throws RestException {
		return new OfferPicturesRestClient();
	}

	@Override
	public String getURLpath() {
		return "/offerPictureses";
	}
    
}
