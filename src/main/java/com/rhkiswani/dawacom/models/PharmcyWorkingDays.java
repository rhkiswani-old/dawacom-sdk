/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rhkiswani.dawacom.models;

import java.io.Serializable;
import java.util.Date;

import com.rhkiswani.api.consumer.rest.AbstractEntity;
import com.rhkiswani.api.consumer.rest.AbstractRestClient;
import com.rhkiswani.api.consumer.rest.annotations.IdField;
import com.rhkiswani.api.consumer.rest.exceptions.RestException;
import com.rhkiswani.dawacom.rest.PharmcyWorkingDaysClient;

/**
 *
 * @author mohamed
 */
public class PharmcyWorkingDays extends AbstractEntity<PharmcyWorkingDays> implements Serializable {
    private static final long serialVersionUID = 1L;
    @IdField
    private Integer pharmcyDayId;
    private Integer pharmcyId;
    private Integer dayId;
    private Date openTime;
    private Date closeTime;

    public PharmcyWorkingDays() {
    }

    public PharmcyWorkingDays(Integer pharmcyDayId) {
        this.pharmcyDayId = pharmcyDayId;
    }

    public PharmcyWorkingDays(Integer pharmcyDayId, int pharmcyId, int dayId) {
        this.pharmcyDayId = pharmcyDayId;
        this.pharmcyId = pharmcyId;
        this.dayId = dayId;
    }

    public Integer getPharmcyDayId() {
        return pharmcyDayId;
    }

    public void setPharmcyDayId(Integer pharmcyDayId) {
        this.pharmcyDayId = pharmcyDayId;
    }

    public Integer getPharmcyId() {
        return pharmcyId;
    }

    public void setPharmcyId(Integer pharmcyId) {
        this.pharmcyId = pharmcyId;
    }

    public Integer getDayId() {
        return dayId;
    }

    public void setDayId(Integer dayId) {
        this.dayId = dayId;
    }

    public Date getOpenTime() {
        return openTime;
    }

    public void setOpenTime(Date openTime) {
        this.openTime = openTime;
    }

    public Date getCloseTime() {
        return closeTime;
    }

    public void setCloseTime(Date closeTime) {
        this.closeTime = closeTime;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (pharmcyDayId != null ? pharmcyDayId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PharmcyWorkingDays)) {
            return false;
        }
        PharmcyWorkingDays other = (PharmcyWorkingDays) object;
        if ((this.pharmcyDayId == null && other.pharmcyDayId != null) || (this.pharmcyDayId != null && !this.pharmcyDayId.equals(other.pharmcyDayId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.rhkiswani.dawacom.models.PharmcyWorkingDays[ pharmcyDayId=" + pharmcyDayId + " ]";
    }

	@Override
	public AbstractRestClient<PharmcyWorkingDays> getRestClient() throws RestException {
		return new PharmcyWorkingDaysClient();
	}

	@Override
	public String getURLpath() {
		return "/pharmcyWorkingDays";
	}
    
}
