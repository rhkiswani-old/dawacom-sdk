/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rhkiswani.dawacom.models;

import java.io.Serializable;
import java.util.Date;

import com.rhkiswani.api.consumer.rest.AbstractEntity;
import com.rhkiswani.api.consumer.rest.AbstractRestClient;
import com.rhkiswani.api.consumer.rest.annotations.IdField;
import com.rhkiswani.api.consumer.rest.exceptions.RestException;
import com.rhkiswani.dawacom.rest.AdviceRestClient;

/**
 *
 * @author mohamed
 */
public class Adviaces extends AbstractEntity<Adviaces> implements Serializable {
    private static final long serialVersionUID = 1L;
    @IdField
    private Integer adviceId;
    private String adviceSubject;
    private String adviceBody;
    private Date publishDate;
    private int isPublished;
    private String addedByUserId;
    private Date addedDate;
    private byte[] adviceImg;
    

    public Adviaces() {
    }

    public Adviaces(Integer adviceId) {
        this.adviceId = adviceId;
    }

    public Adviaces(Integer adviceId, String adviceSubject, String adviceBody, Date publishDate, int isPublished) {
        this.adviceId = adviceId;
        this.adviceSubject = adviceSubject;
        this.adviceBody = adviceBody;
        this.publishDate = publishDate;
        this.isPublished = isPublished;
    }

    public Integer getAdviceId() {
        return adviceId;
    }

    public void setAdviceId(Integer adviceId) {
        this.adviceId = adviceId;
    }

    public String getAdviceSubject() {
        return adviceSubject;
    }

    public void setAdviceSubject(String adviceSubject) {
        this.adviceSubject = adviceSubject;
    }

    public String getAdviceBody() {
        return adviceBody;
    }

    public void setAdviceBody(String adviceBody) {
        this.adviceBody = adviceBody;
    }

    public Date getPublishDate() {
        return publishDate;
    }

    public void setPublishDate(Date publishDate) {
        this.publishDate = publishDate;
    }

    public int getIsPublished() {
        return isPublished;
    }

    public void setIsPublished(int isPublished) {
        this.isPublished = isPublished;
    }

    public String getAddedByUserId() {
        return addedByUserId;
    }

    public void setAddedByUserId(String addedByUserId) {
        this.addedByUserId = addedByUserId;
    }

    public Date getAddedDate() {
        return addedDate;
    }

    public void setAddedDate(Date addedDate) {
        this.addedDate = addedDate;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (adviceId != null ? adviceId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Adviaces)) {
            return false;
        }
        Adviaces other = (Adviaces) object;
        if ((this.adviceId == null && other.adviceId != null) || (this.adviceId != null && !this.adviceId.equals(other.adviceId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.rhkiswani.dawacom.models.Adviaces[ adviceId=" + adviceId + " ]";
    }

	@Override
	public AbstractRestClient<Adviaces> getRestClient() throws RestException {
		return new AdviceRestClient();
	}

	@Override
	public String getURLpath() {
		return "/advices";
	}

	public byte[] getAdviceImg() {
		return adviceImg;
	}

	public void setAdviceImg(byte[] adviceImg) {
		this.adviceImg = adviceImg;
	}
    
	
}
