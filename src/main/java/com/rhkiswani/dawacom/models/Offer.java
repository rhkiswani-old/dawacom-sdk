/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rhkiswani.dawacom.models;

import java.io.Serializable;
import java.util.Date;

import com.rhkiswani.api.consumer.rest.AbstractEntity;
import com.rhkiswani.api.consumer.rest.AbstractRestClient;
import com.rhkiswani.api.consumer.rest.annotations.IdField;
import com.rhkiswani.api.consumer.rest.exceptions.RestException;
import com.rhkiswani.dawacom.rest.OffersRestClient;

/**
 *
 * @author mohamed
 */
public class Offer extends AbstractEntity<Offer> implements Serializable {
	private static final long serialVersionUID = 1L;
	@IdField
	private Integer offerId;
	private String offerSubject;
	private String offerBody;
	private Date publishDate;
	private int isPublished;
	private String addedByUserId;
	private Date addedDate;
	private Integer offerTypeId;
	private byte[] defaultImage;

	public Offer() {
	}

	public Offer(Integer offerId) {
		this.offerId = offerId;
	}

	public Offer(Integer offerId, String offerSubject, String offerBody, Date publishDate, int isPublished) {
		this.offerId = offerId;
		this.offerSubject = offerSubject;
		this.offerBody = offerBody;
		this.publishDate = publishDate;
		this.isPublished = isPublished;
	}

	public Integer getOfferId() {
		return offerId;
	}

	public void setOfferId(Integer offerId) {
		this.offerId = offerId;
	}

	public String getOfferSubject() {
		return offerSubject;
	}

	public void setOfferSubject(String offerSubject) {
		this.offerSubject = offerSubject;
	}

	public String getOfferBody() {
		return offerBody;
	}

	public void setOfferBody(String offerBody) {
		this.offerBody = offerBody;
	}

	public Date getPublishDate() {
		return publishDate;
	}

	public void setPublishDate(Date publishDate) {
		this.publishDate = publishDate;
	}

	public int getIsPublished() {
		return isPublished;
	}

	public void setIsPublished(int isPublished) {
		this.isPublished = isPublished;
	}

	public String getAddedByUserId() {
		return addedByUserId;
	}

	public void setAddedByUserId(String addedByUserId) {
		this.addedByUserId = addedByUserId;
	}

	public Date getAddedDate() {
		return addedDate;
	}

	public void setAddedDate(Date addedDate) {
		this.addedDate = addedDate;
	}

	public Integer getOfferTypeId() {
		return offerTypeId;
	}

	public void setOfferTypeId(Integer offerTypeId) {
		this.offerTypeId = offerTypeId;
	}

	public void setPublished(int isPublished) {
		this.isPublished = isPublished;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (offerId != null ? offerId.hashCode() : 0);
		return hash;
	}

	
	public byte[] getDefaultImage() {
		return defaultImage;
	}

	public void setDefaultImage(byte[] defaultImage) {
		this.defaultImage = defaultImage;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are
		// not set
		if (!(object instanceof Offer)) {
			return false;
		}
		Offer other = (Offer) object;
		if ((this.offerId == null && other.offerId != null) || (this.offerId != null && !this.offerId.equals(other.offerId))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return offerSubject;
	}

	@Override
	public AbstractRestClient<Offer> getRestClient() throws RestException {
		return new OffersRestClient();
	}

	@Override
	public String getURLpath() {
		return "/offers";
	}

}
