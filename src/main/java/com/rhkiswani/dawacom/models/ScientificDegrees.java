/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rhkiswani.dawacom.models;

import java.io.Serializable;
import java.util.Collection;

import com.rhkiswani.api.consumer.rest.AbstractEntity;
import com.rhkiswani.api.consumer.rest.AbstractRestClient;
import com.rhkiswani.api.consumer.rest.annotations.IdField;
import com.rhkiswani.api.consumer.rest.exceptions.RestException;
import com.rhkiswani.dawacom.rest.ScientificDegreesRestClient;

/**
 *
 * @author mohamed
 */
public class ScientificDegrees extends AbstractEntity<ScientificDegrees> implements Serializable {
    private static final long serialVersionUID = 1L;
    @IdField
    private Integer scientificDegreeId;
    private String degreeName;

    public ScientificDegrees() {
    	
    }

    public ScientificDegrees(Integer scientificDegreeId) {
        this.scientificDegreeId = scientificDegreeId;
    }

    public Integer getScientificDegreeId() {
        return scientificDegreeId;
    }

    public void setScientificDegreeId(Integer scientificDegreeId) {
        this.scientificDegreeId = scientificDegreeId;
    }

    public String getDegreeName() {
        return degreeName;
    }

    public void setDegreeName(String degreeName) {
        this.degreeName = degreeName;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (scientificDegreeId != null ? scientificDegreeId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ScientificDegrees)) {
            return false;
        }
        ScientificDegrees other = (ScientificDegrees) object;
        if ((this.scientificDegreeId == null && other.scientificDegreeId != null) || (this.scientificDegreeId != null && !this.scientificDegreeId.equals(other.scientificDegreeId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return degreeName ;
    }

	@Override
	public AbstractRestClient<ScientificDegrees> getRestClient() throws RestException {
		return new ScientificDegreesRestClient();
	}

	@Override
	public String getURLpath() {
		return "/scientificDegrees";
	}
    
}
