/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rhkiswani.dawacom.models;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

import com.rhkiswani.api.consumer.rest.AbstractEntity;
import com.rhkiswani.api.consumer.rest.AbstractRestClient;
import com.rhkiswani.api.consumer.rest.annotations.IdField;
import com.rhkiswani.api.consumer.rest.exceptions.RestException;
import com.rhkiswani.dawacom.rest.ClientQuestionsRestClient;

/**
 *
 * @author mohamed
 */
public class ClientQuestions extends AbstractEntity<ClientQuestions> implements Serializable {
    private static final long serialVersionUID = 1L;
    @IdField
    private Integer clientQuestionId;
    private String questionSubject;
    private String questionBody;
    private Date askedDate;
    private int isAnswered;
    private Integer pharmcyId;
    private Integer clientId;

    public ClientQuestions() {
    }

    public ClientQuestions(Integer clientQuestionId) {
        this.clientQuestionId = clientQuestionId;
    }

    public ClientQuestions(Integer clientQuestionId, Date askedDate, int isAnswered) {
        this.clientQuestionId = clientQuestionId;
        this.askedDate = askedDate;
        this.isAnswered = isAnswered;
    }

    public Integer getClientQuestionId() {
        return clientQuestionId;
    }

    public void setClientQuestionId(Integer clientQuestionId) {
        this.clientQuestionId = clientQuestionId;
    }

    public String getQuestionSubject() {
        return questionSubject;
    }

    public void setQuestionSubject(String questionSubject) {
        this.questionSubject = questionSubject;
    }

    public String getQuestionBody() {
        return questionBody;
    }

    public void setQuestionBody(String questionBody) {
        this.questionBody = questionBody;
    }

    public Date getAskedDate() {
        return askedDate;
    }

    public void setAskedDate(Date askedDate) {
        this.askedDate = askedDate;
    }

    public int getIsAnswered() {
        return isAnswered;
    }

    public void setIsAnswered(int isAnswered) {
        this.isAnswered = isAnswered;
    }

    public Integer getPharmcyId() {
        return pharmcyId;
    }

    public void setPharmcyId(Integer pharmcyId) {
        this.pharmcyId = pharmcyId;
    }

    public Integer getClientId() {
        return clientId;
    }

    public void setClientId(Integer clientId) {
        this.clientId = clientId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (clientQuestionId != null ? clientQuestionId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ClientQuestions)) {
            return false;
        }
        ClientQuestions other = (ClientQuestions) object;
        if ((this.clientQuestionId == null && other.clientQuestionId != null) || (this.clientQuestionId != null && !this.clientQuestionId.equals(other.clientQuestionId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.rhkiswani.dawacom.models.ClientQuestions[ clientQuestionId=" + clientQuestionId + " ]";
    }

	@Override
	public AbstractRestClient<ClientQuestions> getRestClient() throws RestException {
		return new ClientQuestionsRestClient();
	}

	@Override
	public String getURLpath() {
		return "/clientQuestions";
	}
    
}
