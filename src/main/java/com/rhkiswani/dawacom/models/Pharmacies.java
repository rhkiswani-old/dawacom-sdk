/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rhkiswani.dawacom.models;

import java.io.Serializable;
import java.util.Collection;

import com.rhkiswani.api.consumer.rest.AbstractEntity;
import com.rhkiswani.api.consumer.rest.AbstractRestClient;
import com.rhkiswani.api.consumer.rest.annotations.IdField;
import com.rhkiswani.api.consumer.rest.exceptions.RestException;
import com.rhkiswani.dawacom.rest.PharmaciesRestClient;

/**
 *
 * @author mohamed
 */
public class Pharmacies extends AbstractEntity<Pharmacies> implements Serializable {
	private static final long serialVersionUID = 1L;
	@IdField
	private Integer pharmacyId;
	private String pharmcyName;
	private Double pharmcyLongitude;
	private Double pharmcyLatitude;
	private String tel;
	private String email;
    private Integer cityId;


	public Pharmacies() {

	}

	public Pharmacies(Integer pharmacyId) {
		this.pharmacyId = pharmacyId;
	}

	public Pharmacies(Integer pharmacyId, String pharmcyName, String tel) {
		this.pharmacyId = pharmacyId;
		this.pharmcyName = pharmcyName;
		this.tel = tel;
	}

	public Integer getPharmacyId() {
		return pharmacyId;
	}

	public void setPharmacyId(Integer pharmacyId) {
		this.pharmacyId = pharmacyId;
	}

	public String getPharmcyName() {
		return pharmcyName;
	}

	public void setPharmcyName(String pharmcyName) {
		this.pharmcyName = pharmcyName;
	}

	public Double getPharmcyLongitude() {
		return pharmcyLongitude;
	}

	public void setPharmcyLongitude(Double pharmcyLongitude) {
		this.pharmcyLongitude = pharmcyLongitude;
	}

	public Double getPharmcyLatitude() {
		return pharmcyLatitude;
	}

	public void setPharmcyLatitude(Double pharmcyLatitude) {
		this.pharmcyLatitude = pharmcyLatitude;
	}

	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (pharmacyId != null ? pharmacyId.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are
		// not set
		if (!(object instanceof Pharmacies)) {
			return false;
		}
		Pharmacies other = (Pharmacies) object;
		if ((this.pharmacyId == null && other.pharmacyId != null) || (this.pharmacyId != null && !this.pharmacyId.equals(other.pharmacyId))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "com.rhkiswani.dawacom.models.Pharmacies[ pharmacyId=" + pharmacyId + " ]";
	}

	@Override
	public AbstractRestClient<Pharmacies> getRestClient() throws RestException {
		return new PharmaciesRestClient();
	}

	@Override
	public String getURLpath() {
		return "/pharmacies";
	}

	public Integer getCityId() {
		return cityId;
	}

	public void setCityId(Integer cityId) {
		this.cityId = cityId;
	}

}
