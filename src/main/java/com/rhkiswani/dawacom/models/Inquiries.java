/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rhkiswani.dawacom.models;

import java.io.Serializable;
import java.util.Date;

import com.rhkiswani.api.consumer.rest.AbstractEntity;
import com.rhkiswani.api.consumer.rest.AbstractRestClient;
import com.rhkiswani.api.consumer.rest.annotations.IdField;
import com.rhkiswani.api.consumer.rest.exceptions.RestException;
import com.rhkiswani.dawacom.rest.InquiriesRestClient;

/**
 *
 * @author mohamed
 */
public class Inquiries extends AbstractEntity<Inquiries> implements Serializable {
	private static final long serialVersionUID = 1L;
	@IdField
	private Integer id;
	private String inquirysubject;
	private String inquiryBody;
private Date creationDate;
	private String createdBy;
	private int priority;
	private Integer inquiriesCategoryId;

	public Inquiries() {
	}

	public Inquiries(Integer id) {
		this.id = id;
	}

	public Inquiries(Integer id, String inquirysubject, String inquiryBody, Date creationDate, String createdBy, int priority) {
		this.id = id;
		this.inquirysubject = inquirysubject;
		this.inquiryBody = inquiryBody;
		//this.creationDate = creationDate;
		this.createdBy = createdBy;
		this.priority = priority;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getInquirysubject() {
		return inquirysubject;
	}

	public void setInquirysubject(String inquirysubject) {
		this.inquirysubject = inquirysubject;
	}

	public String getInquiryBody() {
		return inquiryBody;
	}

	public void setInquiryBody(String inquiryBody) {
		this.inquiryBody = inquiryBody;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public int getPriority() {
		return priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}

	public Integer getInquiriesCategoryId() {
		return inquiriesCategoryId;
	}

	public void setInquiriesCategoryId(Integer inquiriesCategoryId) {
		this.inquiriesCategoryId = inquiriesCategoryId;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are
		// not set
		if (!(object instanceof Inquiries)) {
			return false;
		}
		Inquiries other = (Inquiries) object;
		if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return inquirysubject;
	}

	@Override
	public AbstractRestClient<Inquiries> getRestClient() throws RestException {
		return new InquiriesRestClient();
	}

	@Override
	public String getURLpath() {
		return "/inquirieses";
	}

}
