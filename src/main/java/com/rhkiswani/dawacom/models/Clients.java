/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rhkiswani.dawacom.models;

import java.io.Serializable;
import java.util.Collection;

import com.rhkiswani.api.consumer.rest.AbstractEntity;
import com.rhkiswani.api.consumer.rest.AbstractRestClient;
import com.rhkiswani.api.consumer.rest.annotations.IdField;
import com.rhkiswani.api.consumer.rest.exceptions.RestException;
import com.rhkiswani.dawacom.rest.ClientsRestClient;

/**
 *
 * @author mohamed
 */
public class Clients extends AbstractEntity<Clients> implements Serializable {
    private static final long serialVersionUID = 1L;
    @IdField
    private Integer clientId;
    private String clientFirstName;
    private String clientLastName;
    private Double clientLongitude;
    private Double clientLatitude;
    private String clientTelNum;
    private int isEnabled;
    private Integer favoritePharmcyId;

    public Clients() {
    }

    public Clients(Integer clientId) {
        this.clientId = clientId;
    }

    public Clients(Integer clientId, String clientFirstName, String clientLastName, int isEnabled) {
        this.clientId = clientId;
        this.clientFirstName = clientFirstName;
        this.clientLastName = clientLastName;
        this.isEnabled = isEnabled;
    }

    public Integer getClientId() {
        return clientId;
    }

    public void setClientId(Integer clientId) {
        this.clientId = clientId;
    }

    public String getClientFirstName() {
        return clientFirstName;
    }

    public void setClientFirstName(String clientFirstName) {
        this.clientFirstName = clientFirstName;
    }

    public String getClientLastName() {
        return clientLastName;
    }

    public void setClientLastName(String clientLastName) {
        this.clientLastName = clientLastName;
    }

    public Double getClientLongitude() {
        return clientLongitude;
    }

    public void setClientLongitude(Double clientLongitude) {
        this.clientLongitude = clientLongitude;
    }

    public Double getClientLatitude() {
        return clientLatitude;
    }

    public void setClientLatitude(Double clientLatitude) {
        this.clientLatitude = clientLatitude;
    }

    public String getClientTelNum() {
        return clientTelNum;
    }

    public void setClientTelNum(String clientTelNum) {
        this.clientTelNum = clientTelNum;
    }

    public int getIsEnabled() {
        return isEnabled;
    }

    public void setIsEnabled(int isEnabled) {
        this.isEnabled = isEnabled;
    }


    public Integer getFavoritePharmcyId() {
        return favoritePharmcyId;
    }

    public void setFavoritePharmcyId(Integer favoritePharmcyId) {
        this.favoritePharmcyId = favoritePharmcyId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (clientId != null ? clientId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Clients)) {
            return false;
        }
        Clients other = (Clients) object;
        if ((this.clientId == null && other.clientId != null) || (this.clientId != null && !this.clientId.equals(other.clientId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.rhkiswani.dawacom.models.Clients[ clientId=" + clientId + " ]";
    }

	@Override
	public AbstractRestClient<Clients> getRestClient() throws RestException {
		return new ClientsRestClient();
	}

	@Override
	public String getURLpath() {
		return "/clients";
	}
    
}
