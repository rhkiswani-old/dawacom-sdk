/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rhkiswani.dawacom.models;

import java.io.Serializable;

import com.rhkiswani.api.consumer.rest.AbstractEntity;
import com.rhkiswani.api.consumer.rest.AbstractRestClient;
import com.rhkiswani.api.consumer.rest.annotations.IdField;
import com.rhkiswani.api.consumer.rest.exceptions.RestException;
import com.rhkiswani.dawacom.rest.OfferTypeRestClient;

/**
 *
 * @author mohamed
 */
public class OfferType extends AbstractEntity<OfferType> implements Serializable {
	private static final long serialVersionUID = 1L;
	@IdField
	private Integer offerTypeId;
	private String offerType;
	private byte[] offerTypeImage;

	public OfferType() {
	}

	public OfferType(Integer offerTypeId) {
		this.offerTypeId = offerTypeId;
	}

	public OfferType(Integer offerTypeId, String offerType) {
		this.offerTypeId = offerTypeId;
		this.offerType = offerType;
	}

	public Integer getOfferTypeId() {
		return offerTypeId;
	}

	public void setOfferTypeId(Integer offerTypeId) {
		this.offerTypeId = offerTypeId;
	}

	public String getOfferType() {
		return offerType;
	}

	public void setOfferType(String offerType) {
		this.offerType = offerType;
	}

	public byte[] getOfferTypeImage() {
		return offerTypeImage;
	}

	public void setOfferTypeImage(byte[] offerTypeImage) {
		this.offerTypeImage = offerTypeImage;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (offerTypeId != null ? offerTypeId.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are
		// not set
		if (!(object instanceof OfferType)) {
			return false;
		}
		OfferType other = (OfferType) object;
		if ((this.offerTypeId == null && other.offerTypeId != null) || (this.offerTypeId != null && !this.offerTypeId.equals(other.offerTypeId))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return offerType;
	}

	@Override
	public AbstractRestClient<OfferType> getRestClient() throws RestException {
		return new OfferTypeRestClient();
	}

	@Override
	public String getURLpath() {
		return "/offerTypes";
	}

}
