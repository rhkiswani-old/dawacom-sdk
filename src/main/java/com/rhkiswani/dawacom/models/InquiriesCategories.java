/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rhkiswani.dawacom.models;

import java.io.Serializable;
import java.util.Date;

import com.rhkiswani.api.consumer.rest.AbstractEntity;
import com.rhkiswani.api.consumer.rest.AbstractRestClient;
import com.rhkiswani.api.consumer.rest.annotations.IdField;
import com.rhkiswani.api.consumer.rest.exceptions.RestException;
import com.rhkiswani.dawacom.rest.InquiriesCategoriesRestClient;

/**
 *
 * @author mohamed
 */
public class InquiriesCategories extends AbstractEntity<InquiriesCategories> implements Serializable {
	private static final long serialVersionUID = 1L;
	@IdField
	private Integer categoryId;
	private String categoryName;
	private Date creationDate;
	private String createdBy;

	public InquiriesCategories() {
	}

	public InquiriesCategories(Integer categoryId) {
		this.categoryId = categoryId;
	}

	public InquiriesCategories(Integer categoryId, String categoryName, Date creationDate, String createdBy) {
		this.categoryId = categoryId;
		this.categoryName = categoryName;
		this.creationDate = creationDate;
		this.createdBy = createdBy;
	}

	public Integer getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (categoryId != null ? categoryId.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are
		// not set
		if (!(object instanceof InquiriesCategories)) {
			return false;
		}
		InquiriesCategories other = (InquiriesCategories) object;
		if ((this.categoryId == null && other.categoryId != null) || (this.categoryId != null && !this.categoryId.equals(other.categoryId))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return categoryName;
	}

	@Override
	public AbstractRestClient<InquiriesCategories> getRestClient() throws RestException {
		return new InquiriesCategoriesRestClient();
	}

	@Override
	public String getURLpath() {
		return "/inquiriesCategorieses";
	}

}
