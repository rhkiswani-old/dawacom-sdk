/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rhkiswani.dawacom.models;

import java.io.Serializable;
import java.util.Date;

import com.rhkiswani.api.consumer.rest.AbstractEntity;
import com.rhkiswani.api.consumer.rest.AbstractRestClient;
import com.rhkiswani.api.consumer.rest.annotations.IdField;
import com.rhkiswani.api.consumer.rest.exceptions.RestException;
import com.rhkiswani.dawacom.rest.CitiesRestClient;
import com.rhkiswani.dawacom.rest.OffersRestClient;

/**
 *
 * @author anasalabed
 */

public class Cities  extends AbstractEntity<Cities> implements Serializable {
    private static final long serialVersionUID = 1L;
    @IdField
    private Integer cityId;
    
    private String cityName;
    
    private Date creationDate;
    
    private String createdBy;
  
    public Cities() {
    }

    public Cities(Integer cityId) {
        this.cityId = cityId;
    }

    public Cities(Integer cityId, String cityName, Date creationDate) {
        this.cityId = cityId;
        this.cityName = cityName;
        this.creationDate = creationDate;
    }

    public Integer getCityId() {
        return cityId;
    }

    public void setCityId(Integer cityId) {
        this.cityId = cityId;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cityId != null ? cityId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Cities)) {
            return false;
        }
        Cities other = (Cities) object;
        if ((this.cityId == null && other.cityId != null) || (this.cityId != null && !this.cityId.equals(other.cityId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.aa.jusmartapp.beans.aa.Cities[ cityId=" + cityId + " ]";
    }
	@Override
	public AbstractRestClient<Cities> getRestClient() throws RestException {
		return new CitiesRestClient();
	}

	@Override
	public String getURLpath() {
		return "/citieses";
	}
}
