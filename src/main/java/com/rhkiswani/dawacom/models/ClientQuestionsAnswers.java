/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rhkiswani.dawacom.models;

import java.io.Serializable;
import java.util.Date;

import com.rhkiswani.api.consumer.rest.AbstractEntity;
import com.rhkiswani.api.consumer.rest.AbstractRestClient;
import com.rhkiswani.api.consumer.rest.annotations.IdField;
import com.rhkiswani.api.consumer.rest.exceptions.RestException;
import com.rhkiswani.dawacom.rest.ClientQuestionsAnswerRestClient;

/**
 *
 * @author mohamed
 */
public class ClientQuestionsAnswers extends AbstractEntity<ClientQuestionsAnswers> implements Serializable {
    private static final long serialVersionUID = 1L;
    @IdField
    private Integer answerId;
    private String answer;
    private Date answeredDate;
    private Integer pharmcyMemberId;
    private Integer clientQuestionId;

    public ClientQuestionsAnswers() {
    	
    }

    public ClientQuestionsAnswers(Integer answerId) {
        this.answerId = answerId;
    }

    public ClientQuestionsAnswers(Integer answerId, String answer, Date answeredDate) {
        this.answerId = answerId;
        this.answer = answer;
        this.answeredDate = answeredDate;
    }

    public Integer getAnswerId() {
        return answerId;
    }

    public void setAnswerId(Integer answerId) {
        this.answerId = answerId;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public Date getAnsweredDate() {
        return answeredDate;
    }

    public void setAnsweredDate(Date answeredDate) {
        this.answeredDate = answeredDate;
    }

    public Integer getPharmcyMemberId() {
        return pharmcyMemberId;
    }

    public void setPharmcyMemberId(Integer pharmcyMemberId) {
        this.pharmcyMemberId = pharmcyMemberId;
    }

    public Integer getClientQuestionId() {
        return clientQuestionId;
    }

    public void setClientQuestionId(Integer clientQuestionId) {
        this.clientQuestionId = clientQuestionId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (answerId != null ? answerId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ClientQuestionsAnswers)) {
            return false;
        }
        ClientQuestionsAnswers other = (ClientQuestionsAnswers) object;
        if ((this.answerId == null && other.answerId != null) || (this.answerId != null && !this.answerId.equals(other.answerId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.rhkiswani.dawacom.models.ClientQuestionsAnswers[ answerId=" + answerId + " ]";
    }

	@Override
	public AbstractRestClient<ClientQuestionsAnswers> getRestClient() throws RestException {
		return new ClientQuestionsAnswerRestClient();
	}

	@Override
	public String getURLpath() {
		return "/clientQuestionAnswers";
	}
    
}
