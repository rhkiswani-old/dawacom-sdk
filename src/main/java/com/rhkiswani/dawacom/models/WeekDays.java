/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rhkiswani.dawacom.models;

import java.io.Serializable;

import com.rhkiswani.api.consumer.rest.AbstractEntity;
import com.rhkiswani.api.consumer.rest.AbstractRestClient;
import com.rhkiswani.api.consumer.rest.annotations.IdField;
import com.rhkiswani.api.consumer.rest.exceptions.RestException;
import com.rhkiswani.dawacom.rest.WeekDaysRestClient;

/**
 *
 * @author mohamed
 */
public class WeekDays extends AbstractEntity<WeekDays> implements Serializable {
    private static final long serialVersionUID = 1L;
    @IdField
    private Integer dayId;
    private String dayArabicName;
    private String dayEnglishName;
    private int isBusinessDay;

    public WeekDays() {
    }

    public WeekDays(Integer dayId) {
        this.dayId = dayId;
    }

    public WeekDays(Integer dayId, String dayArabicName, String dayEnglishName, int isBusinessDay) {
        this.dayId = dayId;
        this.dayArabicName = dayArabicName;
        this.dayEnglishName = dayEnglishName;
        this.isBusinessDay = isBusinessDay;
    }

    public Integer getDayId() {
        return dayId;
    }

    public void setDayId(Integer dayId) {
        this.dayId = dayId;
    }

    public String getDayArabicName() {
        return dayArabicName;
    }

    public void setDayArabicName(String dayArabicName) {
        this.dayArabicName = dayArabicName;
    }

    public String getDayEnglishName() {
        return dayEnglishName;
    }

    public void setDayEnglishName(String dayEnglishName) {
        this.dayEnglishName = dayEnglishName;
    }

    public int getIsBusinessDay() {
        return isBusinessDay;
    }

    public void setIsBusinessDay(int isBusinessDay) {
        this.isBusinessDay = isBusinessDay;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (dayId != null ? dayId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof WeekDays)) {
            return false;
        }
        WeekDays other = (WeekDays) object;
        if ((this.dayId == null && other.dayId != null) || (this.dayId != null && !this.dayId.equals(other.dayId))) {
            return false;
        }
        return true;
    }
    @Override
    public String toString() {
        return dayEnglishName;
    }

	@Override
	public AbstractRestClient<WeekDays> getRestClient() throws RestException {
		return new WeekDaysRestClient();
	}

	@Override
	public String getURLpath() {
		return "/weekDayses";
	}
    
}
